#include <iostream>
#include <fstream>
#include <unistd.h> // for usleep

using namespace std;

// Read Temeratures from file
float ReadTemp(string r_file)
{
    float a;
    ifstream myfile(r_file);
    myfile >> a;
    myfile.close();
    return a;
}

// Set value based on ReadTemp 
int SetFanValue(string w_file, int a)
{
    ofstream myfile(w_file);
    myfile << a;
    myfile.close();
    return a;
}


int main()
{
    while (1)
    {
        float temp {ReadTemp("ReadTemp.txt")};
        usleep(1000000); // sleep in microseconds
        
        int max_fan_speed = 256; // maximum fan speed
        int low_fan_speed = static_cast<double>(max_fan_speed) / 100 * temp;
        int high_fan_speed = static_cast<double>(max_fan_speed) / 100 * (temp+20);
        
        if (temp <= 30)
        {
            SetFanValue("SetValue.txt", 0);
        }
        
        if (temp >= 30)
        {
            SetFanValue("SetValue.txt", low_fan_speed);
        }
        
        if (temp >= 50)
        {
            SetFanValue("SetValue.txt", low_fan_speed);
        }
        
        if (temp >= 60)
        {
            SetFanValue("SetValue.txt", high_fan_speed);
        }
        
        if (temp >= 70)
        {
            SetFanValue("SetValue.txt", max_fan_speed);
        }
        
        printf("%f\n", temp);
    }
    
}